# Workaround for targets with the same name as a directory
.PHONY: doc

all: install unit_tests clean

clean:
	cd library && make clean
	cd command_line && make clean
	cd gui && make clean
	$(MAKE) -C ./doc clean

install:
	cd library && make install
	cd command_line && make install
	cd gui && make install

uninstall:
	-cd command_line && make uninstall
	-cd gui && make uninstall
	-cd library && make uninstall

dev_install:
	cd library && make dev_install
	cd command_line && make dev_install
	cd gui && make dev_install

unit_tests:
	-cd command_line && make unit_tests
	-cd library && make unit_tests

upload:
	-cd library && make upload
	-cd command_line && make upload
	-cd gui && make upload

doc:
	$(MAKE) -C ./doc html
