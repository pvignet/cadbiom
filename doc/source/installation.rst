Installation
============

Foreword
--------

Language used
~~~~~~~~~~~~~

This program is tested on Python 2.7 on a GNU/Linux operating system.
It uses Python bindings to the SAT solver `CryptoMiniSat <https://www.msoos.org/cryptominisat5/>`_,
which comes with the Cadbiom library.

Unfortunately we currently do not plan to port Cadbiom on Python 3.
The first reason is that the first GUI of Cadbiom was developed in 2010 and
is still based on GTK2 (formerly GIMP Toolkit) that works
exclusively with Python 2.7, and sadly, can not be easily ported
without heavy design changes to GTK3 that is specific to Python 3.

.. https://stackoverflow.com/questions/27542828/is-it-possible-to-create-gtk2-x-applications-using-python-3


We use standard Python application packaging by hosting our program on the PyPI platform.
This simple and light but well-proven system allows to quickly deploy and install an application.
The procedure is described in the next chapter.

.. seealso:: `Working with Cadbiom source code <./dev_work_with_source.html#working-with-cadbiom-source-code>`_

..
   Veuillez noter que contrairement à des idées (trop) largement répandues, l'usage de containers
   entraine une complexité tout à fait inutile et chronophage lorsque l'on met en perspective
   les besoins d'un déploiement applicatif de quelques centaines de Ko, maintenu par peu de personnes.
   C'est pourquoi nous utilisons un système de dépendance mature et simple en vertue du principe `KISS <https://fr.wikipedia.org/wiki/Principe_KISS>`_ bien connu en développement logiciel.

Please note that contrary to (too) widespread beliefs, the use of containers leads to a completely unnecessary and time-consuming complexity when putting into perspective the needs of an application deployment of a few hundred Kb, maintained by few people. That's why we use a mature and simple dependency system based on the `KISS <https://fr.wikipedia.org/wiki/Principe_KISS>`_ principle well known in software development.

The Windows problem
~~~~~~~~~~~~~~~~~~~

For a comfortable use on Microsoft Windows systems we recommend the use of virtual machines like Virtual Box.
In a general way, if you need to perform computationally intensive uses,
you will probably want to move to a cluster-like infrastructure running a GNU/Linux operating system.

However, it should be possible to install the software suite under Microsoft Windows via `Cygwin <https://www.cygwin.com/>`_
which offers a large collection of GNU and Open Source tools, and provides on Microsoft Windows,
functionalities similar to a GNU/Linux environment.


We use a sat solver written in C ++, and except for projects like Cygwin,
the porting on Microsoft Windows of native Python 2.7 applications
embedding extensions written in at least C++11 and newer is impossible in a safe way, because it
may lead to erratic behavior of programs and their plugins.

The explanation finds its origin as much in the version of Python 2.7 today (2018) widely depreciated
and in the version of the compiler used at the time to distribute it,
as in the architecture of the (C, C++) libraries under Windows
(whose symbols are not versioned like those in libraries under GNU / Linux)
(more information `here on the Microsoft Developer Network (MSDN) blog <https://blogs.msdn.microsoft.com/vcblog/2014/06/10/the-great-c-runtime-crt-refactoring/>`_).


System requirements
-------------------

CADBIOM is mainly developed in Python 2.7. This Python environment must be installed on
your computer.
Before running Cadbiom, the following system packages have to be installed from the distribution
packages library (or similar, depending on your operating system).
**Most of these packages are already installed on basic GNU/Linux systems.**

As you will see, **most of the packages are required only by the Cadbiom GUI**,
whose installation is optional for simplification purposes.


Debian-like systems (Ubuntu)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- python2.7-dev
- libxml2-dev
- libxslt1-dev (Library providing the Gnome XSLT engine)
- pkg-config (Optional, can fix errors when installing pygraphviz and when config is not loaded, see `Troubleshooting <installation.html#troubleshooting>`_ section)
- libgraphviz-dev (GUI for graph layout algorithms)
- python-gtk2 (GUI)
- python-glade2 (GUI)
- python-gtksourceview2 (GUI)
- python-tk (GUI)

You can install these dependencies with the following shell command::

  $ sudo apt-get install python-gtksourceview2 python2.7-dev libxml2-dev libxslt1-dev \
  libxslt1-dev libgraphviz-dev pkg-config python-glade2 python-gtk2 python-tk

Red Hat-like systems (Fedora/CentOS)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- python-devel
- libxml-devel
- redhat-rpm-config (lxml config)
- libxslt-devel (Library providing the Gnome XSLT engine)
- graphviz-devel (GUI layouts)
- pygtk2 (GUI)
- pygtk2-libglade (GUI)
- pygtksourceview (GUI)

You can install these dependencies with the following command::

  $ sudo dnf install python-devel libxml-devel redhat-rpm-config libxslt-devel graphviz-devel \
  pygtk2 pygtk2-libglade pygtksourceview



Setting up a virtual environment
--------------------------------

As always, the use of a Python virtual environment
(via `virtualenvwrapper <https://docs.python-guide.org/dev/virtualenvs/>`_) is **strongly advised**.

This is not a mandatory step but it is a good and a **simple** practice to separate projects
from each other in order to avoid conflicts between dependencies.

* Install virtualenvwrapper:

.. code-block:: bash

   $ pip install --user virtualenvwrapper

* Edit your ``~/.bashrc`` or ``~/.zshrc`` file to source the ``virtualenvwrapper.sh`` script with these lines:

.. code-block:: bash

   $ export PATH=$PATH:~/.local/bin
   $ export WORKON_HOME=~/.virtualenvs
   $ mkdir -p $WORKON_HOME
   $ # The location of this script may vary depending on your GNU/Linux distro
   $ # and depending of your installation procedure with pip.
   $ # See ~/.local/bin/ or /usr/bin
   $ source ~/.local/bin/virtualenvwrapper.sh

* Restart your terminal or run:

.. code-block:: bash

   $ source ~/.bashrc

* Create your virtualenv:

.. code-block:: bash

   $ mkvirtualenv cadbiom -p /usr/bin/python2.7

* Later, if you want to work in the virtualenv:

.. code-block:: bash

   $ workon cadbiom


Install the released version
----------------------------

Cadbiom packages are available on PyPI (Python Package Index), the official third-party
software repository for Python language.

* `Library <https://pypi.python.org/pypi/cadbiom>`_
* `Command line <https://pypi.python.org/pypi/cadbiom-cmd>`_
* `GUI <https://pypi.python.org/pypi/cadbiom-gui>`_

You can install these packages with the following command on all systems with a Python environment with ``pip``:

.. code-block:: bash

   $ pip install cadbiom-cmd cadbiom-gui

.. note:: Don't forget to add the flag ``--user`` to the command above if you don't use
   virtual environment or if you do not have root privileges on your system.

.. note:: The package of the library itself (``cadbiom``) is automatically installed
  as a dependency when the gui or the command line is installed via pip.


At this point 2 commands are available in your shell:

- ``cadbiom``: Launch the GUI program.
- ``cadbiom_cmd``: Launch the command line program.

You can now add a shortcut to the desktop menu by following the directives in the chapter
`Install a desktop shortcut <./installation.html#id3>`_.


Install the development version
-------------------------------

Install from sources
~~~~~~~~~~~~~~~~~~~~

If you have Git installed on your system, it is also possible to install the development
version of Cadbiom.

Before installing the development version, you may need to uninstall the standard version
of Cadbiom using ``pip``:

.. code-block:: bash

   $ pip uninstall cadbiom cadbiom-gui cadbiom-cmd

Then do:

.. code-block:: bash

   $ git clone https://gitlab.inria.fr/pvignet/cadbiom
   $ cd cadbiom
   $ make dev_install


The ``make dev_install`` command uses ``pip install -e .[dev]`` command which allows
you to follow the development branch as it changes by creating links in the right places
and installing the command line scripts to the appropriate locations.

Please note that your changes in the code are directly usable without having to reinstall the packages.

Then, if you want to update Cadbiom at any time, in the same directory do:

.. code-block:: bash

   $ git pull


Uninstall
~~~~~~~~~

Just do:

.. code-block:: bash

   $ make uninstall


Testing
~~~~~~~

Cadbiom uses the Python `pytest <https://pytest.org/>`_ testing package.

You can test the packages from the source directory with:

.. code-block:: bash

   $ make unit_tests


Very outdated release (archived)
--------------------------------

To download Cadbiom on your Linux distribution, click on the following link: `Download <annexe/cadbiom.tar.gz>`_.
Please note that this version is still available on the `INRIA Gitlab (branch 1.1.x) <https://gitlab.inria.fr/pvignet/cadbiom/tree/1.1.x>`_.

You will have to install the following packages:

- python-pygraphviz
- python-lxml
- python-networkx
- python-gtksourceview2

If you encounter some problems with the precompiled solver (`2bits <annexe/solver_32.tar>`_ or `64bits <annexe/solver_64.tar.gz>`_), we provide the sources of the `2.9 (recommended) <annexe/source_solver_2_9.tar.gz>`_ and `2.7 <annexe/source_solver_2_7.tar.gz>`_ versions.
The solver CryptoMiniSat is an LGPL-licenced SAT solver, more information on `cryptominisat2 <http://www.msoos.org/cryptominisat2/>`_.


Install a desktop shortcut
--------------------------

The GUI module allows to install a shortcut in the desktop menu with the help of the following command:

.. code-block:: bash

    $ cadbiom --install_desktop_shortcut

Please note that this will work only on GNU/Linux desktop environments like KDE, GNOME and their derivatives.
These systems agreed on an interoperable configuration file format describing how a particular program is to be launched,
how it appears in menus, etc.
See `Desktop Entry Specification <https://specifications.freedesktop.org/desktop-entry-spec/latest/>`_.


Troubleshooting
---------------

pip command is missing
~~~~~~~~~~~~~~~~~~~~~~
On some very old systems you may have to install also the system package ``python-pip``.

matplotlib is missing
~~~~~~~~~~~~~~~~~~~~~
To be solved: Some (deprecated) functions may require matplotlib::

  $ pip install matplotlib==1.5

pygraphviz error: ``_graphviz.so: undefined symbol``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
When installing pygraphviz on some systems, you may have this error::

  _graphviz.so: undefined symbol: Agundirected

...which is solved by::

  $ pip install --upgrade pygraphviz --install-option="--include-path=/usr/include/graphviz" \
  --install-option="--library-path=/usr/lib/graphviz/"
