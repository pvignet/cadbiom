***********
GUI package
***********

Graphical User Interface
========================

GUI main class - Charter
------------------------
.. automodule:: cadbiom_gui.gt_gui.charter
   :members:

Graph views - ChartView
-----------------------
.. automodule:: cadbiom_gui.gt_gui.chart_view
   :members:

MVC management - EditMVC
------------------------
.. automodule:: cadbiom_gui.gt_gui.edit_mvc
   :members:

GUI controllers
---------------
.. automodule:: cadbiom_gui.gt_gui.chart_controler
   :members:

GUI auxiliary widgets - CharterInfo
-----------------------------------
.. automodule:: cadbiom_gui.gt_gui.charter_info
   :members:

GUI auxiliary widgets - Miscellaneous
-------------------------------------
.. automodule:: cadbiom_gui.gt_gui.chart_misc_widgets
   :members:

Property checking - controllers & widgets
-----------------------------------------
.. automodule:: cadbiom_gui.gt_gui.chart_checker.chart_checker_controler
   :members:

Simulation - controllers & widgets
----------------------------------
.. automodule:: cadbiom_gui.gt_gui.chart_simulator.chart_simul_controler
   :members:

Statical analysis - controllers & widgets
-----------------------------------------
.. automodule:: cadbiom_gui.gt_gui.chart_static.chart_stat_controler
   :members:

Tools
=====

Search areas management
-----------------------
.. automodule:: cadbiom_gui.gt_gui.utils.listDisplay
   :members:

Tabs decoration
---------------
.. automodule:: cadbiom_gui.gt_gui.utils.notebookUtils
   :members:

Error reporters
---------------
.. automodule:: cadbiom_gui.gt_gui.utils.reporter
   :members:

Text editors
------------
.. automodule:: cadbiom_gui.gt_gui.utils.text_page
   :members:

Dialog boxes
------------
.. automodule:: cadbiom_gui.gt_gui.utils.warn
   :members:

Graph Editor Design
===================

Drawing routines
----------------
.. automodule:: cadbiom_gui.gt_gui.graphics.drawing_style
   :members:

Layout routines
---------------
.. automodule:: cadbiom_gui.gt_gui.layout
   :members: