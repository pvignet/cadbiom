Developer overview
==================

Setup the local copy of the code
--------------------------------

1. If you are a new contributor, you have to fork the repository
   (`https://gitlab.inria.fr/pvignet/cadbiom <https://gitlab.inria.fr/pvignet/cadbiom>`_), clone it,
   and then add an upstream repository.

   More information on: `GitLab doc: forking workflow <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>`_.

   .. note::
      Please note that the currently (2017-2020) active branch of development is named ``pypi_packaging``.
      The ``master`` branch is the stable release branch; please avoid proposing pull-requests on this branch except for bugfixes.


   .. code-block:: bash

      $ # Clone your fork
      $ git clone -b pypi_packaging git@gitlab.inria.fr:<your_username>/cadbiom.git
      $ # Add upstream repository
      $ git remote add upstream git@gitlab.inria.fr:pvignet/cadbiom.git


   Now, you should have 2 remote repositories named (``git branch``):

   - upstream, which refers to the Cadbiom repository
   - origin, which refers to your personal fork

.. new line

| 2. Develop your contribution:

   Pull the latest changes from upstream:

   .. code-block:: bash

      $ git checkout <branch_of_reference>
      $ git pull upstream <branch_of_reference>

   Create a branch for the feature you want to work on; the branch name should be explicit and if possible related to an
   opened issue (Ex: Issue number *000* in the following).

   .. code-block:: bash

      $ git checkout -b bugfix-#000

   **Make small commits with explicit messages about why you do things as you progress** (``git add`` and ``git commit``).

   **Cover your code with unit tests and run them before submitting your contribution.**

.. new line

| 3. Submit your contribution:

   Push your changes to your fork:

   .. code-block:: bash

      $ git push origin bugfix-#000

   Go to the GitLab website. The new branch and a green pull-request button should appear.
   To facilitate reviewing and approval, it is strongly encouraged to add a full description of
   your changes to the pull-request.


Build environment setup
-----------------------

Once you’ve cloned your fork of the Cadbiom repository, you should set up a Python development environment tailored for Cadbiom.

See the chapter `Setting up a virtual environment <./installation.html#setting-up-a-virtual-environment>`_ of the installation process.

Then see the chapter `Install the development version <./installation.html#install-the-development-version>`_.


Bugs
----

Please `report bugs on GitLab <https://gitlab.inria.fr/pvignet/cadbiom/issues>`_.
