********************
Command line package
********************

Tools
=====

Models
------

.. automodule:: cadbiom_cmd.tools.models
   :members:

Graphs
------

.. automodule:: cadbiom_cmd.tools.graphs
   :members:

Solutions
---------

.. automodule:: cadbiom_cmd.tools.solutions
   :members:

Display, compare, and query a model
===================================

.. automodule:: cadbiom_cmd.models
   :members:

Merge Minimal Accessibility Conditions
======================================

.. automodule:: cadbiom_cmd.solution_merge
   :members:

Handle generated files
======================

.. automodule:: cadbiom_cmd.solution_sort
   :members:

Search Minimal Accessibility Conditions
=======================================

.. automodule:: cadbiom_cmd.solution_search
   :members:

Make an interaction graph based on molecules of interest
========================================================

.. automodule:: cadbiom_cmd.interaction_graph
   :members: