******************
Command line usage
******************

.. argparse::
   :filename: ../command_line/cadbiom_cmd/cadbiom_cmd.py
   :func: main
   :prog: cadbiom_cmd
