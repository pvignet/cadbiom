***************
Library package
***************

Global settings
===============

.. automodule:: cadbiom.commons
   :members:

Dynamical analysis
==================

Dynamic system
--------------
.. automodule:: cadbiom.models.clause_constraints.CLDynSys
   :members:

Unfolding and solving engine
----------------------------
.. automodule:: cadbiom.models.clause_constraints.mcl.CLUnfolder
   :members:

Dynamic analysis
----------------
.. automodule:: cadbiom.models.clause_constraints.mcl.MCLAnalyser
   :members:

Query
-----
.. automodule:: cadbiom.models.clause_constraints.mcl.MCLQuery
   :members:

Solutions representation
------------------------
.. automodule:: cadbiom.models.clause_constraints.mcl.MCLSolutions
   :members:

Tests
-----

CLUnfolder
~~~~~~~~~~
.. automodule:: cadbiom.models.clause_constraints.mcl.TestCLUnfolder
   :members:

MCLAnalyser
~~~~~~~~~~~
.. automodule:: cadbiom.models.clause_constraints.mcl.TestMCLAnalyser
   :members:

MCLTranslators
~~~~~~~~~~~~~~
.. automodule:: cadbiom.models.clause_constraints.mcl.TestMCLTranslators
   :members:

Models
======

Chart model
-----------

.. automodule:: cadbiom.models.guard_transitions.chart_model
   :members:
.. automodule:: cadbiom.models.biosignal.sig_expr
   :members:

Translators
-----------

BCX models
~~~~~~~~~~
.. automodule:: cadbiom.models.guard_transitions.translators.chart_xml
   :members:

PID models
~~~~~~~~~~
.. automodule:: cadbiom.models.guard_transitions.translators.chart_xml_pid
   :members:

Analyser
--------

Static analysis
~~~~~~~~~~~~~~~
.. automodule:: cadbiom.models.guard_transitions.analyser.static_analysis
   :members:

Visitors of guarded transitions models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. automodule:: cadbiom.models.guard_transitions.analyser.ana_visitors
   :members:

Simulator
---------

.. automodule:: cadbiom.models.guard_transitions.simulator.chart_simul_elem
   :members:
.. automodule:: cadbiom.models.guard_transitions.simulator.translators.gt_visitors
   :members:
.. automodule:: cadbiom.models.guard_transitions.simulator.chart_simul
   :members:

Tests
-----

PathExtractor
~~~~~~~~~~~~~
.. automodule:: cadbiom.models.guard_transitions.TestModelExtraction
   :members:

Static analysis
~~~~~~~~~~~~~~~
.. automodule:: cadbiom.models.guard_transitions.analyser.TestStaticAnalysis
   :members:
