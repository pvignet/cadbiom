Working with Cadbiom source code
================================

The source code is divided into 3 Python packages found in 3 folders at the root of the repository:

==================  ===============  =======================================
Directory           Python package   Description
==================  ===============  =======================================
``./gui``           ``cadbiom-gui``  Graphical User Interface
``./command_line``  ``cadbiom-cmd``  Command line
``./library``       ``cadbiom``      Library used by the 2 previous packages
==================  ===============  =======================================

All the code is developped in Python language.

However some particularly costly functions in computation time have been written in C.
These functions are included in an internal extension of the library.

The extension is compiled during installation or directly downloaded without compiling if your
system supports **Python wheels**.
This is a *relatively new standard* for the distribution of Python packages
available for Linux, Windows or macOS with ``pip >= 1.4`` and ``setuptools >= 0.8``.
This system is a faster and secure way to install native C extension packages.

.. seealso:: `https://pythonwheels.com/ <https://pythonwheels.com/>`_


.. note::
   All existing modules belonging to the above packages
   is not (yet) fully described in the following technical documentation.

   Indeed, only the essential modules, and especially those that have been
   sufficiently reviewed and documented, are mentioned.


Technical documentation
=======================

.. toctree::
   :maxdepth: 5

   command_line_doc
   gui_doc
   library_doc
