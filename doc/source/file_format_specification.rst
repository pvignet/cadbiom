Cadbiom File Format Specification
=================================

Models
------

Cadbiom models are written in XML (Extensible Markup Language).
The current encoding is ASCII, for reasons of ease of maintenance because the language used and the interface remain in Python2 and GTK2.
This means that **non-ASCII characters are currently not allowed** in these files.


Example of model::

  <?xml version = "1.0" encoding="ASCII" standalone="yes" ?>
  <model xmlns="http://cadbiom.genouest.org/" name="example_model">
   <CSimpleNode name="A" xloc="0.287050359712" yloc="0.321867321867"/>
   <CSimpleNode name="B" xloc="0.287019215796" yloc="0.431203931203"/>
   <transition ori="A" ext="B" event="_h_1" condition="" action="" fact_ids="[]"/>
  </model>


Naming
~~~~~~
Model files should have a .bcx extension.

MIME Type
~~~~~~~~~
``application/xml`` or ``text/plain`` (depending on the version, the prologue may be absent).

Body
~~~~
The root element is a :term:`tag` <model>.

Tags for nodes
~~~~~~~~~~~~~~
Type of tags are:

- ``<CSimpleNode>``
- ``<CStartNode>``

Each node :term:`tag` have these attributes:

- ``name``: (mandatory) the name of the node. The name must be unique in the whole model;
- ``xloc``: (optional) x coordinate of the node for graphical representation in the graph editor of Cadbiom;
- ``yloc``: (optional) y coordinate;


Tag for transitions
~~~~~~~~~~~~~~~~~~~
Type of :term:`tag` is:

- ``<transition>``

:term:`Attributes<attribute>`:

- ``ori``: (mandatory) the name of the consumed node which is at the left of the transition.
- ``ext``: (mandatory) the name of the producted node which is at theright of the transition.
- ``event``: (mandatory) the name of the event/clock; 1 event may share multiple transitions.
  Also, an event name can be a logical formula for complex transitions; this kind of formula
  may involve time operators like ``default`` or ``when`` used to describe transitions shared by multiple events
  under various conditions;
- ``condition``: (mandatory) A logical formula whose satisfiability is necessary for the transition to take place.
  A condition can be an empty string.
- ``action``: (optional): todo
- ``fact_ids``: (deprecated) A list of publication IDs for tracability. The content of this attribute is now in metadata.


Metadata content :term:`elements<element>`:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Metadata for nodes and transitions are stored in their XML :term:`elements<element>` between each star-tag and end-tag.
Metadata of Cadbiom entities is serialized as JSON (http://www.json.org) and is accessible from the
Cadbiom framework via the attribute ``note`` of these objects.


Minimal Activation Condition files
----------------------------------

These files store solutions from the Cadbiom framework.

Naming
~~~~~~
Should have a .txt extension.

MIME Type
~~~~~~~~~
``text/plain``

We destinguish 2 types of files: *complete mac* files and *mac* files.

In *complete mac* files each solution is composed by the set of boundaries **and** the trajectory which allowed to produce it.
A set of boundaries is a line with names of places separated by a space.
Each step of the trajectory is present on an independent line. A step is composed of one or more event (s).
Solutions are stored one after the other without separator or line feed.

Example::

  Jnk1_nucleus JUN_nucleus_gene
  % _h_1015
  % _h_323
  % _h_2910

In *mac* files, only the set of boundaries is present.
Sets are stored one after the other without separator or line feed.

Example::

  Jnk1_nucleus JUN_nucleus_gene
  cortisol JNK1_2_cytoplasm JUN_nucleus_gene


Glossary
--------

.. glossary::

   tag
     A tag is a markup construct that begins with < and ends with >.
     2 flavors can be encountered:

     - start-tag, such as ``<section>``;
     - end-tag, such as ``</section>``;

   element
     An element is a logical document component that either begins with a start-tag and ends with a matching end-tag.
     The characters between the start-tag and end-tag, if any, are the element's content, and may contain markup,
     including other elements, which are called child elements.

   attribute
     An attribute is a markup construct consisting of a name–value pair that exists within a start-tag or empty-element tag.
