.. CADBIOM documentation master file, created by
   sphinx-quickstart on Fri Mar 23 18:00:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Overview of CADBIOM |version|
=============================

CADBIOM (Computer Aided Design of Biological Models) is an open source modelling software.
Based on Guarded transition semantic, it gives a formal framework to help the modelling of
biological systems such as cell signalling networks.

Cadbiom provides:

- ability to study the behaviour of guarded transitions systems by generating scenarios
  (trajectory is a sequence of states of biomolecule);

- tools for the study of the structure and dynamics of biological networks;

- a command line and a graphical user interfaces to build and explore models,
  query the dynamics of these models, and work with the trajectories produced.

Free software
-------------

Cadbiom is freely available on cadbiom.genouest.org, distributed
under the terms of the GNU General Public License.
The website also provides a broad range of informations, model library
and documentation.

Availability
------------

The user manual is available in `html <../../manual/manual.html>`_ or `pdf <../../manual/manual.pdf>`_ formats.

The main repository is available on the `INRIA Gitlab <https://gitlab.inria.fr/pvignet/cadbiom/>`_

Cadbiom packages are available on PyPI (Python Package Index), the official third-party
software repository for Python language.

- `Library <https://pypi.python.org/pypi/cadbiom>`_
- `Command line <https://pypi.python.org/pypi/cadbiom-cmd>`_
- `GUI <https://pypi.python.org/pypi/cadbiom-gui>`_

History
-------

Cadbiom was born was born at the end of 2009. It has been extensively reviewed,
rewritten and accompanied by technical documentation since 2016.

Documentation
-------------

.. toctree::
   :maxdepth: 6

   installation
   command_line_usage
   tutorial

   file_format_specification

   dev_documentation

   license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
