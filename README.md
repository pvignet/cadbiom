CADBIOM (Computer Aided Design of Biological Models) is an open source modelling software.
Based on Guarded transition semantic, it gives a formal framework to help the modelling of
biological systems such as cell signaling network.

Official website: [link](http://cadbiom.genouest.org/)
Full documentation: [link](http://cadbiom.genouest.org/doc/cadbiom/index.html)

# Installation

## System requirements

CADBIOM is mainly developed in Python 2.7.
Before running Cadbiom, the following system packages have to be installed from
the distribution packages library (or similar, depending on your operating system).<br>
**Most of these packages are already installed on basic GNU/Linux systems.**

### Debian-like systems (Ubuntu):

* python2.7-dev
* libxml2-dev
* libxslt1-dev (Library providing the Gnome XSLT engine)
* pkg-config (Fix errors when installing pygraphviz and when config is not loaded)
* libgraphviz-dev (GUI layouts)
* python-gtk2 (GUI)
* python-glade2 (GUI)
* python-gtksourceview2 (GUI)
* python-tk (GUI)

You can install these dependencies with the following command:

    sudo apt-get install python-gtksourceview2 python2.7-dev libxml2-dev libxslt1-dev \
    libxslt1-dev libgraphviz-dev pkg-config python-glade2 python-gtk2 python-tk

On some old systems you may have to install also `python-pip`.
When installing pygraphviz on some systems, you may have this error:

    _graphviz.so: undefined symbol: Agundirected

...which is solved by:

    pip install --upgrade pygraphviz --install-option="--include-path=/usr/include/graphviz" \
    --install-option="--library-path=/usr/lib/graphviz/"

### Red Hat-like systems (Fedora/CentOS)

* python-devel
* libxml-devel
* redhat-rpm-config (lxml config)
* libxslt-devel (Library providing the Gnome XSLT engine)
* graphviz-devel (GUI layouts)
* pygtk2 (GUI)
* pygtk2-libglade (GUI)
* pygtksourceview (GUI)

You can install these dependencies with the following command:

    sudo dnf install python-devel libxml-devel redhat-rpm-config libxslt-devel graphviz-devel \
    pygtk2 pygtk2-libglade pygtksourceview


## Python requirements

When `cadbiom` library is installed, the following Python packages
**are automatically** installed from PyPI repository:

* lxml
* networkx
* pygraphviz
* numpy
* pycryptosat


### Virtual environment

As always, the use of a Python virtual environment
(via [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)) is **strongly advised**.

This is not a mandatory step but it is a good practice to use virtual environments
to separate projects from each other.

* Install virtualenvwrapper:

```bash
pip install virtualenvwrapper
```

* Edit your `.bashrc` or `.zshrc` file to source the `virtualenvwrapper.sh` script with these lines:

```bash
export WORKON_HOME=~/.virtualenvs
mkdir -p $WORKON_HOME
source /usr/bin/virtualenvwrapper.sh
```

The location of this script may vary depending on your Linux distro

* Restart your terminal or run:

```bash
source /usr/bin/virtualenvwrapper.sh
```

* Create your virtualenv:

```bash
mkvirtualenv cadbiom -p /usr/bin/python2.7
```

* Later if you want to reactivate the virtualenv:

```bash
workon cadbiom
```


### SAT solver

Cadbiom software requires a SAT solver which is
proposed as a Python wrapper by an independant library (pycryptosat).
**This package is automatically installed as a dependency of the Cadbiom library.**

For now, we maintain and use a non official version of pycryptosat which is compatible
with PyPI (by design, the original software can not and will not be distributed like this).

Here you will find the fork if you want to compile it manually:
[Cryptominisat repository](https://gitlab.inria.fr/pvignet/pycryptosat/tree/5.0.1_cmake_dev)*

And here are the installation commands to compile the pycryptosat package from our sources:

    git clone --branch 5.0.1_cmake_dev git@gitlab.irisa.fr:0000B8EG/pycryptosat.git
    cd pycryptosat
    mkdir build && cd build && cmake ../
    make python_interface_install


### Cadbiom

Cadbiom package is available on PyPI (Python Package Index), the official third-party
software repository for Python language.

* [Library](https://pypi.python.org/pypi/cadbiom>)
* [Command line](https://pypi.python.org/pypi/cadbiom-cmd>)
* [GUI](https://pypi.python.org/pypi/cadbiom-gui)

You can install these packages with the following command on all systems that run Python interpreter:

    pip install cadbiom-cmd cadbiom-gui

The package of the library itself (`cadbiom`) is automatically installed as a dependency
when the gui or the command line is installed via pip.


#### Installation for developers

If you want to install from the sources, the library with its GUI and command line,
just do:

```bash
git clone https://gitlab.inria.fr/pvignet/cadbiom
```

and then:

```bash
make install
```

If you want to make changes in the code without having to reinstall the packages,
it is **strongly advised** to use the following command:

```bash
make dev_install
```

#### Uninstall

Depending on the installation method you have chosen in the previous chapter:

```bash
make uninstall
# or
make dev_uninstall
```

#### Structure of packages

The library contains the cadbiom folder with 4 modules:

    antlr3 (folder)
    bio_models (folder)
    models (folder)

The command line tool exposes the command `cadbiom_cmd` to the current context.

The gui package exposes the command `cadbiom` to the current context.


# Use

## Command line & API

For up-to-date help on how to use the various commands, please visit this page:
[link](http://cadbiom.genouest.org/doc/cadbiom/command_line_usage.html)

For up-to-date help on how to use the various APIs, please visit this page:
[link](http://cadbiom.genouest.org/doc/cadbiom/command_line_doc.html)


## Graphical User Interface

Usage:

    $ cadbiom


## Logging

Logs are stored in the system temporary dictory:

* On Windows, the directories C:\TEMP, C:\TMP, \TEMP, and \TMP, in that order.
* On all other platforms, the directories /tmp, /var/tmp, and /usr/tmp, in that order.
* As a last resort, the current working directory.


# License

CADBIOM is freely available on cadbiom.genouest.org,
and distributed under the terms of the GNU General Public License.
