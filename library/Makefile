# Jar file from https://www.antlr3.org/download/
antlr_tool = antlr-3.5.2-complete.jar
#antlr_tool = antlr-runtime-3.1.3

all: clean compile sdist

clean:
	@echo Clean Python build dir...
	python2.7 setup.py clean --all
	@echo Clean Python distribution dir...
	@-rm -rf dist
	@-rm -rf *egg-info
	@-rm _cadbiom.so

compile:
	@echo Building the library...
	python2.7 setup.py build

sdist:
	@echo Building the distribution package...
	python2.7 setup.py sdist

bdist:
	python setup.py bdist_wheel
	@echo Add manylinux2010 tag
	@echo -e "\e[31mDo this only if you are compiling on a compatible distribution!\e[0m"
	rename 's/mu-linux_x86_64/mu-manylinux2010_x86_64/' dist/*.whl

install:
	@echo Install the package...
	pip install .

uninstall:
	@echo Uninstalling the package...
	pip uninstall -y cadbiom

dev_install:
	@echo Install the package for developers...
	@# Replacement for python setup.py develop which doesn't support extra_require keyword.
	@# Install a project in editable mode.
	pip install -e .[dev]

unit_tests:
	@echo Launch unit tests
	python2.7 setup.py test

upload: clean sdist bdist
	twine upload dist/* -r pypi_inria

regenerate_antlr3_targets:
	java -jar $(antlr_tool) cadbiom/models/biosignal/translators/sigexpr_lexer.g
	java -jar $(antlr_tool) cadbiom/models/biosignal/translators/sigexpr_compiler.g
	java -jar $(antlr_tool) cadbiom/models/guard_transitions/condexp.g
	java -jar $(antlr_tool) cadbiom/models/guard_transitions/translators/pintlang.g
	java -jar $(antlr_tool) cadbiom/models/guard_transitions/translators/cadlang.g