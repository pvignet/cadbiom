# -*- coding: utf-8 -*-
"""Unit tests for the solvers with raw statements"""

from __future__ import unicode_literals
from __future__ import print_function

# Standard imports
import pytest
from packaging import version

# Custom imports
from pycryptosat import Solver
from pycryptosat import __version__ as solver_version
#from pyCryptoMS import CryptoMS


def load_solver(solv,
                initial_constraints, final_constraints, dynamic_constraints):

    for clause in final_constraints:
        solv.add_clause(clause)

    for lcl in dynamic_constraints:
        for clause in lcl:
            solv.add_clause(clause)

    for clause in initial_constraints:
        solv.add_clause(clause)


@pytest.fixture()
def feed_statement1():

    vvars    = []#[21, 23]
    max_sol  = 2

    dynamics = [[[-48, 21], [-48, -21], [48, -21, 21], [-14, 3], [-14, 24], [-3, -24, 14], [24, -22], [12, -22], [-24, -12, 22], [-8, 4], [-8, 22], [-4, -22, 8], [20, -14], [20, -8], [14, 8, -20], [-51, 21, 24], [-51, 21, -20], [51, -21], [51, -24, 20], [-40, 14, 13], [40, -14], [40, -13], [13, -17], [-11, -17], [-13, 11, 17], [25, -18], [17, -18], [-25, -17, 18], [-52, 8, 25], [-52, 8, -18], [52, -8], [52, -25, 18], [-34, 18, 7], [34, -18], [34, -7], [-16, 1], [-16, 23], [-1, -23, 16], [-15, 5], [-15, 23], [-5, -23, 15], [10, -16], [10, -15], [16, 15, -10], [-50, 23], [-50, -10], [50, -23, 10], [-9, 2], [-9, 26], [-2, -26, 9], [-53, 16, 26], [-53, 16, -9], [53, -16], [53, -26, 9], [-19, 6], [-19, 27], [-6, -27, 19], [-54, 15, 27], [-54, 15, -19], [54, -15], [54, -27, 19], [-39, 9, 12], [39, -9], [39, -12], [-38, 19, 11], [38, -19], [38, -11], [-1, 23], [-2, 26], [-3, 24], [-4, 24], [-5, 23], [-6, 27]]]
    initial  = [[-24], [-13], [-25], [-7], [-26], [-27], [-12], [-11]]
    final    = [[34]]

    nb_vars  = 54
    lintsol  = ()

    return max_sol, vvars, initial, final, dynamics, nb_vars, lintsol


@pytest.fixture()
def feed_statement2():

    vvars    = []#[21, 23]
    max_sol  = 2

    dynamics = [[[-48, 21], [-48, -21], [48, -21, 21], [-14, 3], [-14, 24], [-3, -24, 14], [24, -22], [12, -22], [-24, -12, 22], [-8, 4], [-8, 22], [-4, -22, 8], [20, -14], [20, -8], [14, 8, -20], [-51, 21, 24], [-51, 21, -20], [51, -21], [51, -24, 20], [-40, 14, 13], [40, -14], [40, -13], [13, -17], [-11, -17], [-13, 11, 17], [25, -18], [17, -18], [-25, -17, 18], [-52, 8, 25], [-52, 8, -18], [52, -8], [52, -25, 18], [-34, 18, 7], [34, -18], [34, -7], [-16, 1], [-16, 23], [-1, -23, 16], [-15, 5], [-15, 23], [-5, -23, 15], [10, -16], [10, -15], [16, 15, -10], [-50, 23], [-50, -10], [50, -23, 10], [-9, 2], [-9, 26], [-2, -26, 9], [-53, 16, 26], [-53, 16, -9], [53, -16], [53, -26, 9], [-19, 6], [-19, 27], [-6, -27, 19], [-54, 15, 27], [-54, 15, -19], [54, -15], [54, -27, 19], [-39, 9, 12], [39, -9], [39, -12], [-38, 19, 11], [38, -19], [38, -11], [-1, 23], [-2, 26], [-3, 24], [-4, 24], [-5, 23], [-6, 27]], [[-75, 48], [-75, -48], [75, -48, 48], [-41, 30], [-41, 51], [-30, -51, 41], [51, -49], [39, -49], [-51, -39, 49], [-35, 31], [-35, 49], [-31, -49, 35], [47, -41], [47, -35], [41, 35, -47], [-78, 48, 51], [-78, 48, -47], [78, -48], [78, -51, 47], [-67, 41, 40], [67, -41], [67, -40], [40, -44], [-38, -44], [-40, 38, 44], [52, -45], [44, -45], [-52, -44, 45], [-79, 35, 52], [-79, 35, -45], [79, -35], [79, -52, 45], [-61, 45, 34], [61, -45], [61, -34], [-43, 28], [-43, 50], [-28, -50, 43], [-42, 32], [-42, 50], [-32, -50, 42], [37, -43], [37, -42], [43, 42, -37], [-77, 50], [-77, -37], [77, -50, 37], [-36, 29], [-36, 53], [-29, -53, 36], [-80, 43, 53], [-80, 43, -36], [80, -43], [80, -53, 36], [-46, 33], [-46, 54], [-33, -54, 46], [-81, 42, 54], [-81, 42, -46], [81, -42], [81, -54, 46], [-66, 36, 39], [66, -36], [66, -39], [-65, 46, 38], [65, -46], [65, -38], [-28, 50], [-29, 53], [-30, 51], [-31, 51], [-32, 50], [-33, 54]]]
    initial  = [[-24], [-13], [-25], [-7], [-26], [-27], [-12], [-11]]
    final    = [[61]]

    nb_vars  = 81
    lintsol  = ()

    return max_sol, vvars, initial, final, dynamics, nb_vars, lintsol


@pytest.fixture()
def feed_statement3():

    vvars    = []#[21, 23]
    max_sol  = 2

    dynamics = [[[-48, 21], [-48, -21], [48, -21, 21], [-14, 3], [-14, 24], [-3, -24, 14], [24, -22], [12, -22], [-24, -12, 22], [-8, 4], [-8, 22], [-4, -22, 8], [20, -14], [20, -8], [14, 8, -20], [-51, 21, 24], [-51, 21, -20], [51, -21], [51, -24, 20], [-40, 14, 13], [40, -14], [40, -13], [13, -17], [-11, -17], [-13, 11, 17], [25, -18], [17, -18], [-25, -17, 18], [-52, 8, 25], [-52, 8, -18], [52, -8], [52, -25, 18], [-34, 18, 7], [34, -18], [34, -7], [-16, 1], [-16, 23], [-1, -23, 16], [-15, 5], [-15, 23], [-5, -23, 15], [10, -16], [10, -15], [16, 15, -10], [-50, 23], [-50, -10], [50, -23, 10], [-9, 2], [-9, 26], [-2, -26, 9], [-53, 16, 26], [-53, 16, -9], [53, -16], [53, -26, 9], [-19, 6], [-19, 27], [-6, -27, 19], [-54, 15, 27], [-54, 15, -19], [54, -15], [54, -27, 19], [-39, 9, 12], [39, -9], [39, -12], [-38, 19, 11], [38, -19], [38, -11], [-1, 23], [-2, 26], [-3, 24], [-4, 24], [-5, 23], [-6, 27]], [[-75, 48], [-75, -48], [75, -48, 48], [-41, 30], [-41, 51], [-30, -51, 41], [51, -49], [39, -49], [-51, -39, 49], [-35, 31], [-35, 49], [-31, -49, 35], [47, -41], [47, -35], [41, 35, -47], [-78, 48, 51], [-78, 48, -47], [78, -48], [78, -51, 47], [-67, 41, 40], [67, -41], [67, -40], [40, -44], [-38, -44], [-40, 38, 44], [52, -45], [44, -45], [-52, -44, 45], [-79, 35, 52], [-79, 35, -45], [79, -35], [79, -52, 45], [-61, 45, 34], [61, -45], [61, -34], [-43, 28], [-43, 50], [-28, -50, 43], [-42, 32], [-42, 50], [-32, -50, 42], [37, -43], [37, -42], [43, 42, -37], [-77, 50], [-77, -37], [77, -50, 37], [-36, 29], [-36, 53], [-29, -53, 36], [-80, 43, 53], [-80, 43, -36], [80, -43], [80, -53, 36], [-46, 33], [-46, 54], [-33, -54, 46], [-81, 42, 54], [-81, 42, -46], [81, -42], [81, -54, 46], [-66, 36, 39], [66, -36], [66, -39], [-65, 46, 38], [65, -46], [65, -38], [-28, 50], [-29, 53], [-30, 51], [-31, 51], [-32, 50], [-33, 54]], [[-102, 75], [-102, -75], [102, -75, 75], [-68, 57], [-68, 78], [-57, -78, 68], [78, -76], [66, -76], [-78, -66, 76], [-62, 58], [-62, 76], [-58, -76, 62], [74, -68], [74, -62], [68, 62, -74], [-105, 75, 78], [-105, 75, -74], [105, -75], [105, -78, 74], [-94, 68, 67], [94, -68], [94, -67], [67, -71], [-65, -71], [-67, 65, 71], [79, -72], [71, -72], [-79, -71, 72], [-106, 62, 79], [-106, 62, -72], [106, -62], [106, -79, 72], [-88, 72, 61], [88, -72], [88, -61], [-70, 55], [-70, 77], [-55, -77, 70], [-69, 59], [-69, 77], [-59, -77, 69], [64, -70], [64, -69], [70, 69, -64], [-104, 77], [-104, -64], [104, -77, 64], [-63, 56], [-63, 80], [-56, -80, 63], [-107, 70, 80], [-107, 70, -63], [107, -70], [107, -80, 63], [-73, 60], [-73, 81], [-60, -81, 73], [-108, 69, 81], [-108, 69, -73], [108, -69], [108, -81, 73], [-93, 63, 66], [93, -63], [93, -66], [-92, 73, 65], [92, -73], [92, -65], [-55, 77], [-56, 80], [-57, 78], [-58, 78], [-59, 77], [-60, 81]]]
    initial  = [[-24], [-13], [-25], [-7], [-26], [-27], [-12], [-11]]
    final    = [[88]]

    nb_vars  = 108
    lintsol  = ()

    return max_sol, vvars, initial, final, dynamics, nb_vars, lintsol


@pytest.fixture()
def feed_statement4():

    vvars    = [21, 23]
    max_sol  = 2

    dynamics = [[[-48, 21], [-48, -21], [48, -21, 21], [-14, 3], [-14, 24], [-3, -24, 14], [24, -22], [12, -22], [-24, -12, 22], [-8, 4], [-8, 22], [-4, -22, 8], [20, -14], [20, -8], [14, 8, -20], [-51, 21, 24], [-51, 21, -20], [51, -21], [51, -24, 20], [-40, 14, 13], [40, -14], [40, -13], [13, -17], [-11, -17], [-13, 11, 17], [25, -18], [17, -18], [-25, -17, 18], [-52, 8, 25], [-52, 8, -18], [52, -8], [52, -25, 18], [-34, 18, 7], [34, -18], [34, -7], [-16, 1], [-16, 23], [-1, -23, 16], [-15, 5], [-15, 23], [-5, -23, 15], [10, -16], [10, -15], [16, 15, -10], [-50, 23], [-50, -10], [50, -23, 10], [-9, 2], [-9, 26], [-2, -26, 9], [-53, 16, 26], [-53, 16, -9], [53, -16], [53, -26, 9], [-19, 6], [-19, 27], [-6, -27, 19], [-54, 15, 27], [-54, 15, -19], [54, -15], [54, -27, 19], [-39, 9, 12], [39, -9], [39, -12], [-38, 19, 11], [38, -19], [38, -11], [-1, 23], [-2, 26], [-3, 24], [-4, 24], [-5, 23], [-6, 27]], [[-75, 48], [-75, -48], [75, -48, 48], [-41, 30], [-41, 51], [-30, -51, 41], [51, -49], [39, -49], [-51, -39, 49], [-35, 31], [-35, 49], [-31, -49, 35], [47, -41], [47, -35], [41, 35, -47], [-78, 48, 51], [-78, 48, -47], [78, -48], [78, -51, 47], [-67, 41, 40], [67, -41], [67, -40], [40, -44], [-38, -44], [-40, 38, 44], [52, -45], [44, -45], [-52, -44, 45], [-79, 35, 52], [-79, 35, -45], [79, -35], [79, -52, 45], [-61, 45, 34], [61, -45], [61, -34], [-43, 28], [-43, 50], [-28, -50, 43], [-42, 32], [-42, 50], [-32, -50, 42], [37, -43], [37, -42], [43, 42, -37], [-77, 50], [-77, -37], [77, -50, 37], [-36, 29], [-36, 53], [-29, -53, 36], [-80, 43, 53], [-80, 43, -36], [80, -43], [80, -53, 36], [-46, 33], [-46, 54], [-33, -54, 46], [-81, 42, 54], [-81, 42, -46], [81, -42], [81, -54, 46], [-66, 36, 39], [66, -36], [66, -39], [-65, 46, 38], [65, -46], [65, -38], [-28, 50], [-29, 53], [-30, 51], [-31, 51], [-32, 50], [-33, 54]], [[-102, 75], [-102, -75], [102, -75, 75], [-68, 57], [-68, 78], [-57, -78, 68], [78, -76], [66, -76], [-78, -66, 76], [-62, 58], [-62, 76], [-58, -76, 62], [74, -68], [74, -62], [68, 62, -74], [-105, 75, 78], [-105, 75, -74], [105, -75], [105, -78, 74], [-94, 68, 67], [94, -68], [94, -67], [67, -71], [-65, -71], [-67, 65, 71], [79, -72], [71, -72], [-79, -71, 72], [-106, 62, 79], [-106, 62, -72], [106, -62], [106, -79, 72], [-88, 72, 61], [88, -72], [88, -61], [-70, 55], [-70, 77], [-55, -77, 70], [-69, 59], [-69, 77], [-59, -77, 69], [64, -70], [64, -69], [70, 69, -64], [-104, 77], [-104, -64], [104, -77, 64], [-63, 56], [-63, 80], [-56, -80, 63], [-107, 70, 80], [-107, 70, -63], [107, -70], [107, -80, 63], [-73, 60], [-73, 81], [-60, -81, 73], [-108, 69, 81], [-108, 69, -73], [108, -69], [108, -81, 73], [-93, 63, 66], [93, -63], [93, -66], [-92, 73, 65], [92, -73], [92, -65], [-55, 77], [-56, 80], [-57, 78], [-58, 78], [-59, 77], [-60, 81]], [[-129, 102], [-129, -102], [129, -102, 102], [-95, 84], [-95, 105], [-84, -105, 95], [105, -103], [93, -103], [-105, -93, 103], [-89, 85], [-89, 103], [-85, -103, 89], [101, -95], [101, -89], [95, 89, -101], [-132, 102, 105], [-132, 102, -101], [132, -102], [132, -105, 101], [-121, 95, 94], [121, -95], [121, -94], [94, -98], [-92, -98], [-94, 92, 98], [106, -99], [98, -99], [-106, -98, 99], [-133, 89, 106], [-133, 89, -99], [133, -89], [133, -106, 99], [-115, 99, 88], [115, -99], [115, -88], [-97, 82], [-97, 104], [-82, -104, 97], [-96, 86], [-96, 104], [-86, -104, 96], [91, -97], [91, -96], [97, 96, -91], [-131, 104], [-131, -91], [131, -104, 91], [-90, 83], [-90, 107], [-83, -107, 90], [-134, 97, 107], [-134, 97, -90], [134, -97], [134, -107, 90], [-100, 87], [-100, 108], [-87, -108, 100], [-135, 96, 108], [-135, 96, -100], [135, -96], [135, -108, 100], [-120, 90, 93], [120, -90], [120, -93], [-119, 100, 92], [119, -100], [119, -92], [-82, 104], [-83, 107], [-84, 105], [-85, 105], [-86, 104], [-87, 108]]]
    initial  = [[-24], [-13], [-25], [-7], [-26], [-27], [-12], [-11]]
    final    = [[115]]

    nb_vars  = 135
    lintsol  = ((1, -2, -3, -4, -5, -6, -7, -8, -9, 10, -11, -12, -13, -14, -15, 16, -17, -18, -19, -20, 21, -22, 23, -24, -25, -26, -27, -28, 29, -30, -31, -32, -33, -34, -35, 36, -37, -38, -39, -40, -41, -42, -43, -44, -45, -46, -47, -48, -49, -50, 51, -52, 53, -54, -55, -56, 57, 58, -59, -60, -61, 62, -63, -64, -65, 66, -67, 68, -69, -70, -71, -72, -73, 74, -75, 76, -77, 78, -79, -80, -81, -82, -83, -84, -85, -86, -87, -88, -89, -90, -91, -92, 93, 94, -95, -96, -97, 98, 99, -100, -101, -102, -103, -104, -105, 106, -107, -108, -109, -110, -111, -112, -113, -114, 115, -116, -117, -118, -119, 120, 121, -122, -123, -124, -125, -126, -127, -128, -129, -130, -131, -132, -133, -134, -135),)

    return max_sol, vvars, initial, final, dynamics, nb_vars, lintsol


################################################################################
# pyCryptoMS
#def test_statement1(feed_statement1):
#    """
#
#    """
#
#    solv = CryptoMS()
#    load_solver(solv,
#                feed_statement1[2], feed_statement1[3], feed_statement1[4])
#
#    # Check number of variables
#    found = solv.nb_vars()
#    assert feed_statement1[5] == found
#
#    # Solve
#    lintsol = solv.msolve_selected(
#        feed_statement1[0],
#        feed_statement1[1]
#    )
#    assert feed_statement1[6] == lintsol
#
#
#def test_statement2(feed_statement2):
#    """
#
#    """
#
#    solv = CryptoMS()
#    load_solver(solv,
#                feed_statement2[2], feed_statement2[3], feed_statement2[4])
#
#    # Check number of variables
#    found = solv.nb_vars()
#    assert feed_statement2[5] == found
#
#    # Solve
#    lintsol = solv.msolve_selected(
#        feed_statement2[0],
#        feed_statement2[1]
#    )
#    assert feed_statement2[6] == lintsol
#
#
#def test_statement3(feed_statement3):
#    """
#
#    """
#
#    solv = CryptoMS()
#    load_solver(solv,
#                feed_statement3[2], feed_statement3[3], feed_statement3[4])
#
#    # Check number of variables
#    found = solv.nb_vars()
#    assert feed_statement3[5] == found
#
#    # Solve
#    lintsol = solv.msolve_selected(
#        feed_statement3[0],
#        feed_statement3[1]
#    )
#    assert feed_statement3[6] == lintsol
#
#
#def test_statement4(feed_statement4):
#    """
#
#    """
#
#    solv = CryptoMS()
#    load_solver(solv,
#                feed_statement4[2], feed_statement4[3], feed_statement4[4])
#
#    # Check number of variables
#    found = solv.nb_vars()
#    assert feed_statement4[5] == found
#
#    # Solve
#    lintsol = solv.msolve_selected(
#        feed_statement4[0],
#        feed_statement4[1]
#    )
#    assert feed_statement4[6] == lintsol
#

################################################################################
# Pycryptosat
def test_statement1b(feed_statement1):
    """

    """

    solv = Solver()
    load_solver(solv,
                feed_statement1[2], feed_statement1[3], feed_statement1[4])

    # Check number of variables
    if version.parse(solver_version) < version.parse("5.6.9"):
        found = solv.nb_vars()
    else:
        found = solv.nb_vars
    assert feed_statement1[5] == found

    # Solve
    lintsol = solv.msolve_selected(
        feed_statement1[0],
        feed_statement1[1],
        raw=True,
    )
    assert feed_statement1[6] == tuple(lintsol)


def test_statement2b(feed_statement2):
    """

    """

    solv = Solver()
    load_solver(solv,
                feed_statement2[2], feed_statement2[3], feed_statement2[4])

    # Check number of variables
    if version.parse(solver_version) < version.parse("5.6.9"):
        found = solv.nb_vars()
    else:
        found = solv.nb_vars
    assert feed_statement2[5] == found

    # Solve
    lintsol = solv.msolve_selected(
        feed_statement2[0],
        feed_statement2[1],
        raw=True,
    )
    assert feed_statement2[6] == tuple(lintsol)


def test_statement3b(feed_statement3):
    """

    """

    solv = Solver()
    load_solver(solv,
                feed_statement3[2], feed_statement3[3], feed_statement3[4])

    # Check number of variables
    if version.parse(solver_version) < version.parse("5.6.9"):
        found = solv.nb_vars()
    else:
        found = solv.nb_vars
    assert feed_statement3[5] == found

    # Solve
    lintsol = solv.msolve_selected(
        feed_statement3[0],
        feed_statement3[1],
        raw=True,
    )
    assert feed_statement3[6] == tuple(lintsol)


def test_statement4b(feed_statement4):
    """

    """

    solv = Solver()
    load_solver(solv,
                feed_statement4[2], feed_statement4[3], feed_statement4[4])

    # Check number of variables
    if version.parse(solver_version) < version.parse("5.6.9"):
        found = solv.nb_vars()
    else:
        found = solv.nb_vars
    assert feed_statement4[5] == found

    # Solve
    lintsol = solv.msolve_selected(
        feed_statement4[0],
        feed_statement4[1],
        raw=True,
    )
    assert feed_statement4[6] == tuple(lintsol)
