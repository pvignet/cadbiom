CADBIOM (Computer Aided Design of Biological Models) is an open source modelling software.
Based on Guarded transition semantic, it gives a formal framework to help the modelling of
biological systems such as cell signaling network.

**Please refer to the library package for more information.**
